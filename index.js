const express = require("express")
const app = express()
const { PORT = 8080 } = process.env
const passport = require("./lib/passport")

app.use(express.json())

// ... passport
app.use(passport.initialize())


// ... import router
const router = require("./router.js")
app.use(router)

app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`)
})