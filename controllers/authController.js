const passport = require("../lib/passport")
const jwt = require("jsonwebtoken")

module.exports = {
    login: (req, res) => {
        let response = {}

        try {
            let {username, password} = req.body
            if (username == "sabrina@binar.com" && password == "12345") {
                const userData = {
                    username: "sabrina@binar.com",
                    fullname: "Administrator",
                }
                response.token = jwt.sign(userData, "s3cr3t")
            } else {
                res.status(401)
                response.message = "invalid username or password"
            }
        } catch (e) {
            res.status(401)
            response.message = e.message
        }

        res.json(response)
    }
}