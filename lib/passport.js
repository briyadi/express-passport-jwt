const passport = require("passport")
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt")

const userData = {
    username: "sabrina@binar.com",
    fullname: "Administrator",
}

const validator = (payload, done) => {
    if (payload.username == "sabrina@binar.com") {
        return done(null, userData)
    }

    done(new Error("invalid user"), false)
}

const strategy = new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: "s3cr3t"
}, validator)

passport.use(strategy)

module.exports = passport