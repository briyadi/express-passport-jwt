const router = require("express").Router()
const auth = require("./controllers/authController")
const user = require("./controllers/userController")
const authenticator = require("./lib/authenticator")

// .. definisi router
router.post("/api/v1/login", auth.login)
router.get("/api/v1/profile", authenticator, user.profile)
router.use((err, req, res, next) => {
    if (err) {
        return res.status(500).json({message: err.message})
    }

    next()
})

module.exports = router